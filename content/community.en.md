+++
title = "Community"
+++

<article class="hero cards">
    <p>Get involved with Redox! Our friendly community and rich developer environments make Redox an amazing project to work on</p>
    <blockquote>
        We follow the <a href="https://www.rust-lang.org/policies/code-of-conduct">Rust Code Of Conduct</a> for rules in all community/chat channels.
    </blockquote>
    <div class="card-container">
        <div class="card">
            <h2><img src="https://upload.wikimedia.org/wikipedia/commons/c/cb/Element_%28software%29_logo.svg" class="inline" />Matrix</h2>
            <article>
                <p>Official Redox communication takes place via Matrix.</p>
                <a href="https://matrix.to/#/#redox-join:matrix.org"><button>Join us</button></a>
                <details>
                    <summary>Joining Redox</summary>
                    <h3>Choosing a matrix client</h3>
                    <p>To communicate via Matrix, you'll need a client. Most of the Redox community uses <a href="https://element.io">Element</a>, but you may use any client you like.</p>
                    <h3>Joining the Redox community is easy!</h3>
                    <ol>
                        <li><a href="https://matrix.to/#/#redox-join:matrix.org">Join the lobby</a></li>
                        <li>Let us know you'd like an invite</li>
                        <li>A moderator will let you in as quickly as they can</li>
                        <li>Don't forget to leave the lobby once you're invited. There's nothing interesting there, so there's no point in sticking around</li>
                    </ol>
                    <p>We ask that community members communicate in English to aid in keeping communication consistent</p>
                    <h3>Experiencing issues joining?</h3>
                    <p>We alternatively recommend <a href="https://nheko-reborn.github.io/">nheko</a></p>
                </details>
            </article>
        </div>
        <div class="card">
            <h2>Announcements</h2>
            <article>
                <p>Announcements are posted to <a href="https://matrix.to/#/#redox-announcements:matrix.org">the Matrix Annoucements</a> room.</p>
                <p>It's public so you don't need to be signed in to read it.</p>
            </article>
            <a href="https://matrix.to/#/#redox-announcements:matrix.org"><button>Read Annoucements</button></a>
        </div>
        <div class="card">
            <article>
                <h2>Redox Summer of Code</h2>
                <p>The <b>Redox Summer of Code</b> (RSoC) program runs annually when funding permits, and we may participate in other Summer of Code type programs.</p>
                <a href="" class="button primary">Apply now</a>
            </article>
        </div>
    </div>
</article>

## Guidelines for effective communication

Since the introduction of threads in Element, organising conversations by topic is substantially easier. 
We suggest and aks you to open a new thread when asking or responding to a question.

<<<<<<< HEAD
<a href="https://doc.redox-os.org/book/ch13-01-chat.html" class="button">Read More</a>
=======
We do our announcements on the [Announcements](https://matrix.to/#/#redox-announcements:matrix.org) room, it's public and you don't need to login on Matrix to read it.

- #redox-announcements:matrix.org (Use this Matrix room address if you don't want to use the external Matrix link)

## [Chat](https://matrix.to/#/#redox-join:matrix.org)

Matrix is the official way to talk with Redox OS team and community (these rooms are English-only, we don't accept other languages because we don't understand them).

Matrix has several different clients. [Element](https://element.io/) is a commonly used choice, it works on web browsers, Linux, MacOSX, Windows, Android and iOS.

If you have problems with Element, try [Fractal](https://gitlab.gnome.org/World/fractal).

- Join the [Join Requests](https://matrix.to/#/#redox-join:matrix.org) room and don't forget to request an invite to the Redox Matrix space.
- #redox-join:matrix.org (Use this Matrix room address if you don't want to use the external Matrix link)

(We recommend that you leave the "Join Requests" room after your entry on Redox space)

If you want to have a big discussion in our rooms, you should use a Element thread, it's more organized and easy to keep track if more discussions happen on the same room.

You cand find more information on the [Chat](https://doc.redox-os.org/book/chat.html) page.

## [Discord](https://discord.gg/JfggvrHGDY)

We have a Discord server as an alternative for Matrix.

(The Matrix messages are sent to Discord and the Discord messages are sent to Matrix, using a bot)

## [Summer of Code](/rsoc)

The **Redox Summer of Code** (RSoC) program runs annually when funding permits, and we may participate in other Summer of Code type programs.
An overview of our Summer of Code programs and our plans for this year can be found on the [RSoC](/rsoc) page.
Check out our [RSoC Proposal Guide](/rsoc-proposal-how-to) and [Project Suggestions](/rsoc-project-suggestions).
>>>>>>> c8d524ff8fbba749b660969c14f8865b5df0414a

## [GitLab](https://gitlab.redox-os.org/redox-os/redox)

The most formal approach to contact the Redox devs is by submitting issues on the affected repositories. 
This is not limited to bugs, but inconsistencies, translations, or general help questions. 

<<<<<<< HEAD
<a href="https://doc.redox-os.org/book/ch12-01-signing-in-to-gitlab.html" class="button">Request an Account</a>
<a href="https://matrix.to/#/#redox-mrs:matrix.org" class="button">Link MRs</a>
=======
If you want to create an account, read the [Signing in to GitLab](https://doc.redox-os.org/book/signing-in-to-gitlab.html) page.

Once you create an issue don't forget to post the link on the Dev or Support rooms of the chat, because the GitLab email notifications have distractions (service messages or spam) and most developers don't left their GitLab pages open to receive desktop notifications from the web browser (which require a custom setting to receive issue notifications).

By doing this you help us to pay attention to your issues and avoid them to be accidentally forgotten.

If you have ready MRs (merge requests) you must send the links in the [MRs](https://matrix.to/#/#redox-mrs:matrix.org) room. To join this room, you will need to request an invite in the [Join Requests](https://matrix.to/#/#redox-join:matrix.org) room.

By sending a message in the room, your MR will not be forgotten or accumulate conflicts.

## [Lemmy](https://lemmy.world/c/redox)

Our alternative to Reddit, where we post news and community threads.

## [Reddit](https://www.reddit.com/r/Redox/)

If you want a quick look at what's going on and talk about it.

[reddit.com/r/rust](https://www.reddit.com/r/rust) for related Rust news and discussions.

## [Fosstodon](https://fosstodon.org/@redox)

Our alternative to Twitter, where we post news and community threads.

## [Twitter](https://twitter.com/redox_os)

News and community threads.

## [YouTube](https://www.youtube.com/@RedoxOS)

Demos and board meetings.

## [Forum](https://discourse.redox-os.org/)

This is our archived forum with old/classic questions, it's inactive and can only be used for archival purposes. If you have a question, send on Matrix chat.

## [Talks](/talks/)

Redox talks given at various events and conferences.

## Spread the word

Community outreach is an important part of Redox's success. If more people know about Redox, then more contributors are likely to step in, and everyone can benefit from their added expertise. You can make a difference by writing articles, talking to fellow OS enthusiasts, or looking for communities that would be interested in knowing more about Redox.
>>>>>>> c8d524ff8fbba749b660969c14f8865b5df0414a
