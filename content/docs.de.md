+++
title = "Redox Dokumentation"
+++

<article>
<div>
    <div>
        <p>
            Die Dokumentation von Redox wird ständig weiterentwickelt. Wir würden uns über Ihre Hilfe beim Verfassen hochwertiger Dokumentation freuen.
        </p>
        <a href="#"><button>Beitragen</button></a>
    </div>
    <div class="cards">
        <h2>Erste Schritte</h2>
        <article class="hero">
            <h3>Redox OS Buch</h3>
            <p>Das RedoxOS-Buch ist eine fantastische Möglichkeit, die Grundlagen von Redox zu erlernen und ein tieferes Verständnis des Systems zu entwickeln.</p>
            <p>Wir empfehlen dies dringend als Ausgangspunkt für Neulinge und als allgemeine Referenz</p>
            <a href="#" class="button">Redox OS Buch</a>
        </article>
    </div>
    <div class="cards">
        <h2>Verweise</h2>
        <div class="card-container">
            <div class="card">
                <h3><code>redox_syscall</code></h3>
                <article>
                    <p>Diese Crate umschließt die Rohsystemaufrufe, um eine benutzerfreundliche Bibliothek zu erstellen, die Sie zum Implementieren von Schemata verwenden können.</p>
                    <p>Sie werden diese Bibliothek wahrscheinlich nur dann direkt verwenden müssen, wenn Sie selbst ein Schema implementieren. Für reguläre Redox-Software siehe <a href="#">ReLibC</a></p>
                    <a href="#" class="button"><code>redox_syscall</code></a>
                </article>
            </div>
            <div class="card">
                <h3><code>libstd</code></h3>
                <article>
                    <p>Rust-Standardbibliothek</p>
                    <blockquote class="info">
                        <p>
                            Diese Crate wird nicht von Redox OS gepflegt. Sie baut stattdessen auf Arbeiten auf, die von Redox OS gepflegt werden. Weitere Informationen
                         finden Sie unter <a href="#">ReLibC.</a></p>
                    </blockquote>
                    <a href="#" class="button">Rust-Standardbibliothek</a>
                </article>
            </div>
            <div class="card">
                <h3>RFCs</h3>
                <article>
                    <p>Änderungen an Redox anfordern</p>
                    <a href="" class="button">RFCs</a>
                </article>
            </div>
        </div>
    </div>
    <div class="cards">
        <h2>GitLab</h2>
        <div class="card-container">
            <div class="card">
                <h3>Redox-Betriebssystem</h3>
                <article>
                    <p>Das Root-Repository mit allem, was zum Erstellen von Redox OS erforderlich ist</p>
                    <a href="#" class="button">Redox-Betriebssystem</a>
                </article>
            </div>
            <div class="card">
                <h3>Treiber</h3>
                <article>
                    <p>Treiber versorgen die Peripheriegeräte und die interne Hardware mit Strom, die für die Funktion des Computersystems erforderlich sind.</p>
                    <p>Hier ist der Source-Code aller wichtigen Treiber enthalten.</p>
                    <a href="#" class="button">Treiber</a>
                </article>
            </div>
            <div class="card">
                <h3>Ion Shell</h3>
                    <p>Die Ion Shell ist eine POSIX-kompatible Redox-native Shell.</p>
                    <blockquote>
                        <p>
                            Für Ion ist auch ein <a href="#">Buch</a> verfügbar, das einen guten Ausgangspunkt für die Verwendung von Redox als System darstellt. Es bietet außerdem umfangreiche entwicklerorientierte Erweiterungen.
                        </p>
                    </blockquote>
                    <a href="" class="button">Ion Shell</a>
                    <a href="" class="button secondary">Buch</a>
                </div>
            </div>
        </div>
    </div>
</article>

# Häufig gestellte Fragen

<details class="faq">
    <summary><h2>Was ist Redox</h2></summary>
    <article>
        <p>Redox ist ein mikrokernelbasiertes, vollständiges, voll funktionsfähiges und universelles Betriebssystem, das 2015 entwickelt wurde und bei dem der Schwerpunkt auf Sicherheit, Freiheit, Zuverlässigkeit, Korrektheit und Pragmatismus liegt.</p>
        <p>Wo immer möglich, werden die Systemkomponenten in Rust geschrieben und im Benutzerbereich ausgeführt.</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Wo liegt Redox?</h2></summary>
    <article>
        <p>Redox ist Software in Alpha-/Beta-Qualität, da wir neue Funktionen implementieren und gleichzeitig die Fehler beheben.</p>
        <p>Aus diesem Grund ist es noch nicht für den täglichen Gebrauch bereit. Sie können das System ruhig testen, bis es ausgereift ist, und <b>Ihre sensiblen Daten nicht ohne ordnungsgemäße Sicherung speichern.</b></p>
        <p>Die Version 1.0 wird veröffentlicht, sobald alle System-APIs als stabil gelten.</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Was bedeutet der Name <i>Redox</i> ?</h2></summary>
    <article>
        <p>Eine <a href="https://en.wikipedia.org/wiki/Redox">Redoxreaktion</a> führt unter anderem zur Bildung von Rust – was zufällig der Name der Sprache ist, in der Redox geschrieben ist.</p>
        <p>Oh, und es klingt wie Minix und Linux!</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Was ist ein Mikrokernel?</h2></summary>
    <article>
        <p>Ein Mikrokernel ist die nahezu minimale Menge an Software, die die erforderlichen Mechanismen zur Implementierung eines Betriebssystems bereitstellen kann, das mit den höchsten Privilegien des Prozessors ausgeführt wird.</p>
        <p>Dieser Ansatz beim Betriebssystemdesign bietet mehr Stabilität und Sicherheit bei geringen Leistungseinbußen.</p>
        <a href="https://doc.redox-os.org/book/ch04-01-microkernels.html" class="button secondary">Mehr lesen</a>
    </article>
</details>
<details class="faq">
    <summary><h2>Warum ein Mikrokernel?</h2></summary>
    <article>
        <h3>Echte Modularität</h3>
        <p>Sie können viele Systemkomponenten ohne einen Systemneustart modifizieren/ändern, ähnlich wie <a href="https://en.wikipedia.org/wiki/Kpatch">Livepatching</a>, aber sicherer.</p>
        <h3>Fehlerisolierung</h3>
        <p>Die meisten Systemkomponenten laufen im Benutzerbereich eines Mikrokernelsystems. Daher führen Fehler in den meisten Systemkomponenten nicht <a href="https://en.wikipedia.org/wiki/Kernel_panic">zum Absturz des Systems/Kernels</a>.</p>
        <h3>Neustartfreies Design</h3>
        <p>Ein ausgereifter Mikrokernel ändert sich nur sehr wenig (mit Ausnahme von Fehlerbehebungen), sodass Sie Ihr System zum Aktualisieren nicht sehr oft neu starten müssen.</p>
        <p>Da sich die meisten Systemkomponenten im Benutzerbereich befinden, können sie im laufenden Betrieb ersetzt werden, was die Ausfallzeiten für Serveradministratoren reduziert.</p>
        <h3>Einfach zu entwickeln und zu debuggen</h3>
        <p>Die meisten Systemkomponenten werden im Benutzerbereich ausgeführt, was das Testen/Debuggen vereinfacht.</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Warum Rust?</h2></summary>
    <article>
        <h3>Weniger anfällig für Fehler</h3>
        <p>Die restriktive Syntax und Compilervorschläge verringern die Wahrscheinlichkeit von Fehlern erheblich.</p>
        <h3>Weniger anfällig für Datenkorruption</h3>
        <p>Der Rust-Compiler verhindert, dass der Programmierer Speicherfehler und Race Conditions verursacht, wodurch die Wahrscheinlichkeit von Datenbeschädigungsfehlern verringert wird.</p>
        <h3>Verbesserte Sicherheit und Zuverlässigkeit ohne nennenswerte Leistungseinbußen</h3>
        <p>Da der Kernel klein ist, benötigt er für seine Arbeit weniger Speicher. Die begrenzte Kernel-Codegröße hilft uns, auf einen fehlerfreien Status hinzuarbeiten. Vielleicht haben Sie schon vom <a href="https://en.wikipedia.org/wiki/KISS_principle">KISS-Prinzip</a> gehört .</p>
        <h3>Thread-Sicherheit</h3>
        <p>Die C/C++-Unterstützung für Thread-Sicherheit ist ziemlich fragil. Daher ist es sehr einfach, ein Programm zu schreiben, das scheinbar sicher über mehrere Threads ausgeführt werden kann, aber subtile Fehler oder Sicherheitslücken aufweist. Wenn ein Thread auf einen Statusteil zugreift, während ein anderer Thread ihn ändert, kann das gesamte Programm einige wirklich verwirrende und bizarre Fehler aufweisen.</p>
        <p>In Rust lässt sich diese Art von Fehler leicht vermeiden: Das gleiche Typsystem, das uns davon abhält, unsichere Speicherinhalte zu schreiben, verhindert auch, dass wir gefährliche Muster für gleichzeitige Zugriffe schreiben.</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Wie schlägt sich Redox im Vergleich zu anderen Betriebssystemen?</h2></summary>
    <article>
        <p>Wie Redox im Vergleich zu Linux, FreeBSD und Plan 9 abschneidet, können Sie auf <a href="https://doc.redox-os.org/book/ch04-11-features.html">der</a> Seite mit den Features sehen.</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Was ist der Zweck von Redox?</h2></summary>
    <article>
        <p>Das Hauptziel von Redox besteht darin, ein universelles Betriebssystem zu sein und gleichzeitig Sicherheit, Zuverlässigkeit und Korrektheit zu gewährleisten.</p>
        <p>Redox soll eine Alternative zu bestehenden Unix-Systemen (Linux/BSD) sein und die Möglichkeit bieten, die meisten Unix-Programme mit nur einer Neukompilierung oder minimalen Änderungen auszuführen.</p>
        <a href="https://doc.redox-os.org/book/ch01-01-our-goals.html" class="button secondary">Unsere Ziele</a>
    </article>
</details>
<details class="faq">
    <summary><h2>Was kann ich mit Redox machen?</h2></summary>
    <article>
        <p>Da es sich um ein Allzweck-Betriebssystem handelt, können Sie auf den meisten Geräten fast jede Aufgabe mit hoher Leistung/Sicherheit ausführen.</p>
        <p>Redox befindet sich noch in der Entwicklung, daher ist unsere Liste der unterstützten Anwendungen derzeit begrenzt, wächst aber.</p>
        <a href="https://doc.redox-os.org/book/ch01-04-redox-use-cases.html" class="button secondary">Anwendungsfälle</a>
    </article>
</details>
<details class="faq">
    <summary><h2>Was bedeutet UNIX-ähnlich?</h2></summary>
    <article>
        <p>
            Jedes Betriebssystem, das mit der <a href="https://en.wikipedia.org/wiki/Single_UNIX_Specification">Single Unix Specification</a> und <a href="https://en.wikipedia.org/wiki/POSIX">POSIX</a> kompatibel ist, gilt als UNIX-ähnliches Betriebssystem. Sie können eine <a href="https://en.wikipedia.org/wiki/Unix_shell">Shell</a> , das Konzept <a href="https://en.wikipedia.org/wiki/Everything_is_a_file">„Alles ist eine Datei“</a>, Multitasking und Mehrbenutzerunterstützung erwarten.
        </p>
        <a href="https://en.wikipedia.org/wiki/Unix-like" class="button secondary">Mehr lesen</a>
    </article>
</details>
<details class="faq">
    <summary><h2>Wie lässt sich Redox von anderen Systemen inspirieren?</h2></summary>
    <article>
        <h3><a href="http://9p.io/plan9/index.html">Plan 9</a></h3><a href="http://9p.io/plan9/index.html">
        <p>Dieses Betriebssystem von Bell Labs bringt das Konzept „Alles ist eine Datei“ auf die höchste Ebene, indem es die gesamte Systemkommunikation über das Dateisystem abwickelt.</p>
        </a><ul><a href="http://9p.io/plan9/index.html">
            </a><li><a href="http://9p.io/plan9/index.html"></a><a href="https://drewdevault.com/2022/11/12/In-praise-of-Plan-9.html">Drew DeVault erklärt den Plan 9</a></li>
            <li><a href="https://doc.redox-os.org/book/ch05-00-urls-schemes-resources.html">Der Einfluss von Plan 9 auf Redox</a></li>
        </ul>
        <h3><a href="https://minix3.org/">Minix</a></h3>
        <p>
            Das einflussreichste Unix-ähnliche System mit einem Mikrokernel. Es verfügt über erweiterte Funktionen wie Systemmodularität, <a href="https://en.wikipedia.org/wiki/Kernel_panic">Kernel-Panic-</a> Resistenz, Treiber-Reinkarnation, Schutz vor fehlerhaften Treibern und sichere Schnittstellen für <a href="https://en.wikipedia.org/wiki/Inter-process_communication">die Interprozesskommunikation</a> .
        </p>
        <a href="https://doc.redox-os.org/book/ch04-01-microkernels.html">Wie Minix das Redox-Design beeinflusst hat</a>
        <h3><a href="https://sel4.systems/">seL4</a></h3>
        <p>Der einfachste und leistungsstärkste Mikrokernel der Welt.</p>
        <p>Redox folgt demselben Prinzip und versucht, den Kernel-Speicherplatz so klein wie möglich zu halten (durch Verschieben von Komponenten in den Benutzerebene und Reduzierung der Anzahl der Systemaufrufe, Übergabe der Komplexität an die Benutzerebene) und gleichzeitig die Gesamtleistung gut zu halten (durch Reduzierung der Kontextwechsel-Kosten).</p>
        <h3>BSD</h3>
        <p>Diese Unix- <a href="https://en.wikipedia.org/wiki/Research_Unix">Familie</a> beinhaltete mehrere Verbesserungen für Unix-Systeme und die Open-Source-Varianten von BSD fügten dem ursprünglichen System viele Verbesserungen hinzu (wie dies auch bei Linux der Fall war).</p>
        <p>
            <a href="https://www.freebsd.org/">FreeBSD</a> ist das bemerkenswerteste Beispiel, Redox ließ sich bei der Implementierung der Namespaces von <a href="https://man.freebsd.org/cgi/man.cgi?capsicum(4)">Capsicum</a>  
            (einem fähigkeitsbasierten System) und <a href="https://en.wikipedia.org/wiki/Freebsd_jail">Jails</a> (einer Sandbox-Technologie) inspirieren.
        </p>
        <h3><a href="https://www.kernel.org/">Linux</a></h3>
        <p>Der fortschrittlichste monolithische Kernel und das größte Open-Source-Projekt der Welt. Es brachte zahlreiche Verbesserungen und Optimierungen in die Unix-ähnliche Welt.</p>
        <p>Redox versucht, einige der vielen Linux-Leistungsverbesserungen in einem Mikrokernel-Design zu implementieren.</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Welche Programme kann Redox ausführen?</h2></summary>
    <article>
        <p>Redox ist so konzipiert, dass es mit den meisten Unix-, Linux- und POSIX-kompatiblen Anwendungen quellkompatibel ist und lediglich eine Kompilierung erfordert.</p>
        <p>Derzeit müssen die meisten GUI-Anwendungen portiert werden, da wir X11 oder Wayland noch nicht unterstützen. Die folgende Liste wichtiger Software funktioniert jedoch bereits</p>
        <ul>
            <li>GNU Bash</li>
            <li>FFMPEG</li>
            <li>Git</li>
            <li>SDL2</li>
            <li>OpenSSL</li>
            <li>Mesa3D</li>
            <li>GCC</li>
            <li>LLVM</li>
        </ul>
        <a href="https://static.redox-os.org/pkg/x86_64-unknown-redox/" class="buttons">Alle Software</a>
    </article>
</details>
<details class="faq">
    <summary><h2>Wie installiere ich Programme auf Redox?</h2></summary>
    <article>
        <p>Redox verfügt über einen Paketmanager ähnlich <code>apt</code> (Debian) und <code>pkg</code> (FreeBSD). Auf <a href="https://doc.redox-os.org/book/ch02-08-pkg.html">dieser Seite erfahren Sie, wie Sie ihn verwenden.</a></p>
    </article>
</details>
<details class="faq">
    <summary><h2>Welche Redox-Varianten gibt es?</h2></summary>
    <article>
        <p>Mit dem Redox-Build-System können Sie das Aussehen Ihres endgültigen Redox-Images anpassen. Es sind einige Variationen verfügbar, die unterschiedliche Software, Layouts und Funktionen enthalten.</p>
        <table>
            <thead>
                <tr>
                    <td>Variante</td>
                    <td>Zweck</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>server-minimal</code></td>
                    <td>Die minimalste Variante mit einem Basissystem. Richtet sich an eingebettete Geräte, sehr alte Computer und Entwickler.</td>
                </tr>
                <tr>
                    <td><code>desktop-minimal</code></td>
                    <td>Die minimalste Variante mit der enthaltenen Orbital-Desktopumgebung. Für eingebettete Geräte, sehr alte Computer und Entwickler.</td>
                </tr>
                <tr>
                    <td><code>server</code></td>
                    <td>Die Servervariante mit komplettem System und Netzwerktools. Richtet sich an Serveradministratoren, eingebettete Geräte, Low-End-Computer und Entwickler.</td>
                </tr>
                <tr>
                    <td><code>desktop</code></td>
                    <td>Die Standardvariante mit komplettem System, Orbital-Desktopumgebung und nützlichen Tools. Für den täglichen Gebrauch, Produzenten, Entwickler und Gamer.</td>
                </tr>
                <tr>
                    <td><code>dev</code></td>
                    <td>Die Entwicklungsvariante mit komplettem System und Entwicklungstools. Richtet sich an Entwickler.</td>
                </tr>
                <tr>
                    <td><code>demo</code></td>
                    <td>Die Demovariante mit komplettem System, Tools, Playern und Spielen. Richtet sich an Tester, Gamer und Entwickler.</td>
                </tr>
            </tbody>
        </table>
    </article>
</details>
<details class="faq">
    <summary><h2>Welche Geräte unterstützt Redox?</h2></summary>
    <article>
        <p>Es gibt weltweit Milliarden von Geräten mit Hunderten von Modellen und Architekturen. Wir versuchen, Treiber für die am häufigsten verwendeten Geräte zu schreiben, um möglichst viele Benutzer zu unterstützen. Die Unterstützung hängt von der jeweiligen Hardware ab, da einige Treiber gerätespezifisch und andere architekturspezifisch sind.</p>
        <p>Werfen Sie einen Blick auf <a href="https://gitlab.redox-os.org/redox-os/redox/-/blob/master/HARDWARE.md">die Hardware</a> , um alle getesteten Computer anzuzeigen.</p>
        <h3>CPU</h3>
        <ul>
            <li>Intel – 64-Bit (x86_64) und 32-Bit (i686) ab Pentium II mit Einschränkungen.</li>
            <li>AMD – 64-Bit (AMD64) und 32-Bit.</li>
            <li>ARM – 64-Bit (Aarch64) mit Einschränkungen.</li>
        </ul>
        <h3>Hardwareschnittstellen</h3>
        <ul>
            <li>ACPI</li>
            <li>PCI</li>
            <li><i>USB kommt bald</i></li>
        </ul>
        <h3>Video</h3>
        <ul>
            <li>VGA (BIOS)</li>
            <li>GOP (UEFI)</li>
            <li><a href="https://docs.mesa3d.org/drivers/llvmpipe.html">LLVMpipe</a> (OpenGL-CPU-Emulation)</li>
            <li><i>Intel/AMD und andere in der Zukunft</i></li>
        </ul>
        <h3>Audio</h3>
        <ul>
            <li>Intel-Chipsätze</li>
            <li>Realtek-Chipsätze</li>
            <li>PC-Lautsprecher</li>
            <li><i>SoundBlaster kommt bald</i></li>
        </ul>
        <h3>Lagerung</h3>
        <ul>
            <li>IDE (SPOT)</li>
            <li>SATA (AHCI)</li>
            <li>NVMe</li>
            <li><i>USB kommt bald</i></li>
        </ul>
        <h3>Eingang</h3>
        <ul>
            <li>PS/2-Tastaturen, -Mäuse und -Touchpads</li>
            <li><i>USB kommt bald</i></li>
        </ul>
        <h3>Vernetzung</h3>
        <ul>
            <li>Intel Gigabit Ethernet</li>
            <li>Intel 10 Gigabit Ethernet</li>
            <li>Realtek Ethernet</li>
            <li><i>WiFi und Atheros-Ethernet in Kürze verfügbar</i></li>
        </ul>
    </article>
</details>
<details class="faq">
    <summary><h2>Ich habe einen Low-End-Computer. Würde Redox darauf funktionieren?</h2></summary>
    <article>
        <p>Eine CPU ist die komplexeste Maschine der Welt: Selbst die ältesten Prozessoren sind für manche Aufgaben leistungsstark, für andere jedoch nicht.</p>
        <p>Das Hauptproblem bei alten Computern ist die Menge an verfügbarem RAM (sie wurden in einer Zeit verkauft, in der RAM-Chips teuer waren) und das Fehlen von SSE/AVX-Erweiterungen (Programme verwenden sie, um die Algorithmen zu beschleunigen). Aus diesem Grund funktionieren einige moderne Programme möglicherweise nicht oder benötigen viel RAM, um komplexe Aufgaben auszuführen.</p>
        <p>Redox selbst funktioniert normal, wenn die Prozessorarchitektur vom System unterstützt wird, aber Leistung und Stabilität können je nach Programm variieren.</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Auf welchen VMs kann ich Redox ausführen?</h2></summary>
    <article>
        <p>Derzeit lässt sich das Build-System von Redox nur mit Qemu integrieren, aber VirtualBox funktioniert auch. Alle anderen werden nicht unterstützt, daher kann die Leistung variieren.</p>
        <p>In Zukunft könnte der Mikrokernel als Hypervisor fungieren, ähnlich wie <a href="https://xenproject.org/">Xen</a> .</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Wie baue ich Redox?</h2></summary>
    <article>
        <p>Derzeit verfügt Redox über ein Bootstrap-Skript für Pop OS!, Ubuntu, Debian, Fedora, Arch Linux, openSUSE und FreeBSD mit nicht gepflegter Unterstützung für andere Distributionen.</p>
        <p>Wir bieten auch Podman als unsere universelle Kompilierungsmethode an. Es ist der empfohlene Build-Prozess für Nicht-Debian-Systeme, da es Umgebungsprobleme beim Build-Prozess vermeidet.</p>
        <a href="https://doc.redox-os.org/book/ch02-05-building-redox.html" class="button secondary">Buch</a>
        <a href="https://doc.redox-os.org/book/ch02-06-podman-build.html" class="button secondary">Podman</a>
    </article>
</details>
<details class="faq">
    <summary><h2>So beheben Sie im Fehlerfall Probleme mit Ihrem Build</h2></summary>
    <article>
        <p>Lesen Sie <a href="https://doc.redox-os.org/book/ch08-05-troubleshooting.html">diese</a> Seite oder nehmen Sie an unserem <a href="https://doc.redox-os.org/book/ch13-01-chat.html">Redox-Chat</a> teil .</p>
    </article>
</details>
<details class="faq">
    <summary><h2>So melden Sie Fehler bei Redox</h2></summary>
    <article>
        <p>Sehen Sie sich <a href="https://doc.redox-os.org/book/ch12-03-creating-proper-bug-reports.html">diese Seite</a> an und überprüfen Sie die GitLab-Probleme, um zu sehen, ob Ihr Problem gemeldet wurde.</p>
    </article>
</details>
<details class="faq">
    <summary><h2>Wie trage ich zu Redox bei?</h2></summary>
    <article>
        <p>Sie können auf viele Arten zu Redox beitragen. Diese können Sie auf <a href="https://gitlab.redox-os.org/redox-os/redox/blob/master/CONTRIBUTING.md">der Seite „Beitragen“ sehen.</a></p>
    </article>
</details>
<details class="faq">
    <summary><h2>Ich habe ein Problem/eine Frage an das Redox-Team</h2></summary>
    <article>
        <ul>
            <li>Weitere Einzelheiten zu den internen Vorgängen bei Redox finden Sie auf der Seite <a href="/docs/">„Dokumentation“.</a></li>
            <li>Werfen Sie einen Blick in das <a href="https://doc.redox-os.org/book/">Redox-Buch,</a> um zu sehen, ob es Ihre Frage beantwortet oder Ihr Problem löst.</li>
            <li>Wenn Ihre Frage in der Dokumentation oder im Buch nicht beantwortet wird, stellen Sie Ihre Frage oder schildern Sie Ihr Problem im <a href="https://doc.redox-os.org/book/ch13-01-chat.html">Chat</a>.</li>
        </ul>
    </article>
</details>
