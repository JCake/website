+++
title = "Home"
url = "home"
+++

<div class="sticky-container" id="banner-container">
    <div class="header-background"></div>
    <div id="banner">
        <div id="banner-inner">
            <div id="banner-img">
                <img src="/img/redox-orbital/large.png" alt="Orbital, Redox's native desktop environment.">
            </div>
            <div id="banner-call-to-action">
                <h2>Orbital GUI</h2>
                <p>Orbital is Redox's own desktop environment. Here you can see it in action!</p>
                <a href="" class="button external">See Orbital in Action</a>
                <a href="https://gitlab.redox-os.org" class="button gitlab primary">Dive in!</a>
            </div>
        </div>
    </div>
</div>

<hr />

<div>
    <h1>What is Redox OS</h1>
        <p>Redox is a free, open source microkernel Operating System designed to be lightweight, performant and secure.</p>
        <p>
            Being a from-the-ground-up operating system, we have the opportunity to reinvent and modernise everything. We bring you apps written in Rust to provide a more ergonomic and safe environment.
        </p>
    </div>
    <a href="#learn-more" class="button external">Learn More</a>
    <a href="https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md" class="button gitlab">Jump In!</a>
</div>

<div class="advantage">
    <h1><a href="https://en.wikipedia.org/wiki/Microkernel">Microkernel Architecture</a></h1>
    <div class="card">
        <h2>Reduced Attack Surface</h2>
        <article>
            <p>
                With less than 50 000 lines of code,
                the Redox microkernel has a much smaller attack surface compared to Linux,
                and far fewer opportunities for critical vulnerabilities.
            </p>
        </article>
    </div>
    <div class="card">
        <h2>Privilege Constrained</h2>
        <article>
            <p>
                Each task runs in a namespace restricted to the required resources.
            </p>
            <p>Vulnerabilities cannot enable access beyond the limited scope of their task.</p>
        </article>
    </div>
    <div class="card">
        <h2>Stability by Isolation</h2>
        <article>
            <p>
                System components run as separate tasks, and have limited access to kernel memory.
            </p>
            <p>Bugs in drivers or services won't crash the system.</p>
        </article>
    </div>
</div>

<div class="advantage">
    <h1 id="learn-more"><a href="https://www.rust-lang.org/">Written in Rust</a></h1>
    <div class="card">
        <h2>Memory Safe</h2>
        <p>
            Studies show that <a href="https://en.wikipedia.org/wiki/Memory_safety#Impact">up to 70% of vulnerabilities</a> are due to poor memory management. 
            Rust, being designed for absolute memory safety, effectively elimintates this entire class of bugs at compile time, preventing buffer-overflows, use-after-free and race-conditions.
        </p>
    </div>
    <div class="card">
        <h2>Made for Systems</h2>
        <p>
            Rust is strongly typed, features a concurrent, yet safety-first design, and features extensive compile-time checks.<br/>
            It is the most
            <a href="https://www.technologyreview.com/2023/02/14/1067869/rust-worlds-fastest-growing-programming-language/">loved</a>
            and <a href="https://survey.stackoverflow.co/2023/#section-admired-and-desired-programming-scripting-and-markup-languages">admired</a>
            language year after year.
        </p>
    </div>
    <div class="card">
        <h2>Fast</h2>
        <article>
            <p>
                Rust is designed for speed and memory efficiency,
                with inline expansion, static dispatch, and zero-copy sharing.
            </p>
            <p>Rust's performance often outdoes that of C!</p>
        </article>
    </div>
</div>

<div class="advantage">
    <h1>Linux Apps on Redox</h1>
    <div class="card">
        <h2>Cosmic Desktop</h2>
        <article>
            <p>
                <a href="https://blog.system76.com/post/hammering-out-cosmic-features">Cosmic Desktop</a>
                is a new desktop environment for Linux under development by <a href="https://system76.com/">System76</a>.
                Redox includes Cosmic-Files, Cosmic-Edit and Cosmic-Term,
                with plans for more.
        </article>
    </div>
    <div class="card">
        <h2>Written-in-Rust Alternatives</h2>
        <article>
            <p>
                Redox includes many Linux-Rust apps such as
                <ul>
                    <li><a href="https://github.com/uutils/coreutils">uutils</a> coreutils alternative</li>
                    <li><a href="https://helix-editor.com/">Helix</a> editor</li>
                    <li><a href="https://www.nushell.sh/">Nushell</a> command-line shell</li>
                </ul>
                ... and many others.
            </p>
        </article>
    </div>
    <div class="card">
        <h2>Growing compatibility with Linux Apps</h2>
        <article>
            <p>
                Redox provides a source-compatible interface for Linux and BSD programs.
                Currently, dozens of programs work seamlessly,
                and over 1 000 programs and libraries are being ported.
            </p>
        </article>
    </div>
</div>

<div class="advantage">
    <h1>Open Source and Free</h1>
    <div class="card">
        <h2>MIT Licensed</h2>
        <article>
            <p>
                Redox is free to use for any purpose.
                It is developed by a community of volunteers.<br/>
                <a href="/donate">Help support our team!</a>
            </p>
        </article>
    </div>
    <div class="card">
        <h2>Open Source</h2>
        <article>
            <p>
                The Redox software is <a href="https://gitlab.redox-os.org/redox-os/redox">here</a>.</br>
                You can participate in Redox development, report bugs, or make suggestions.
                Join us on <a href="https://doc.redox-os.org/book/ch13-01-chat.html">Matrix Chat</a> if you want to contribute.
            </p>
        </article>
    </div>
    <div class="card">
        <h2>Free Forever</h2>
        <article>
            <p>
                Redox runs on grants and donations.
                Redox is governed by a <a href="https://en.wikipedia.org/wiki/501(c)_organization#501(c)(4)">501(c)(4)</a> Nonprofit.
            </p>
            <a href="./donate" class="button donate primary">Donate</a>
        </article>
    </div>
</div>


<hr />

<div id="sponsors">
    <article class="hero">
        <h1>Sponsors</h1>
        <p>Thanks to our generous sponsors and patrons, we are able to fund programs like our Redox Summer of Code. We are also seeking larger donations to help accelerate our development plans.</p>        
        <a href="./donate" class="button donate primary">Donate</a>
        <div id="sponsors-row" class="row">
            <a href="https://radworks.org" class="block"><img src="/img/logos/radworks.svg"/></a>
        </div>
        <h2>Individual sponsors</h2>
        <p>To this list of course also belong the individuals who have contributed donations to the Redox OS project.</p>
    </article>
</div>
