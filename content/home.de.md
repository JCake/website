+++
title = "Home"
url = "home"
+++

<div class="sticky-container" id="banner-container">
    <div class="header-background"></div>
    <div id="banner">
        <div id="banner-inner">
            <div id="banner-img">
                <img src="/img/redox-orbital/large.png" alt="Orbital, die native Desktopumgebung von Redox.">
            </div>
            <div id="banner-call-to-action">
                <h2>Orbital-Benutzeroberfläche</h2>
                <p>Orbital ist Redox' eigene Desktop-Umgebung. Hier können Sie es in Aktion sehen!</p>
                <a href="" class="button external">Erleben Sie Orbital in Aktion</a>
                <a href="https://gitlab.redox-os.org" class="button gitlab primary">Eintauchen!</a>
            </div>
        </div>
    </div>
</div>

<hr>

<div>
    <h1>Was ist Redox OS</h1>
        <p>Redox ist ein kostenloses, Open-Source-Mikrokernel-Betriebssystem, das auf geringes Gewicht, Leistung und Sicherheit ausgelegt ist.</p>
        <p>
            Da es sich um ein von Grund auf neues Betriebssystem handelt, haben wir die Möglichkeit, alles neu zu erfinden und zu modernisieren. Wir bieten Ihnen in Rust geschriebene Apps, um eine ergonomischere und sicherere Umgebung bereitzustellen.
        </p>
    </div>
    <a href="#learn-more" class="button external">Erfahren Sie mehr</a>
    <a href="https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md" class="button gitlab">Spring rein!</a>
</div>

<div class="advantage">
    <h1><a href="https://en.wikipedia.org/wiki/Microkernel">Mikrokernel-Architektur</a></h1>
    <div class="card">
        <h2>Reduzierte Angriffsfläche</h2>
        <article>
            <p>
                Mit weniger als 50.000 Codezeilen bietet der Redox-Mikrokernel im Vergleich zu Linux eine viel kleinere Angriffsfläche und weitaus weniger Möglichkeiten für kritische Schwachstellen.
            </p>
        </article>
    </div>
    <div class="card">
        <h2>Eingeschränkte Rechte</h2>
        <article>
            <p>
                Jede Aufgabe wird in einem Namespace ausgeführt, der auf die erforderlichen Ressourcen beschränkt ist.
            </p>
            <p>Schwachstellen können keinen Zugriff ermöglichen, der über den begrenzten Rahmen ihrer Aufgabe hinausgeht.</p>
        </article>
    </div>
    <div class="card">
        <h2>Stabilität durch Isolation</h2>
        <article>
            <p>
                Systemkomponenten werden als separate Aufgaben ausgeführt und haben nur eingeschränkten Zugriff auf den Kernelspeicher.
            </p>
            <p>Fehler in Treibern oder Diensten führen nicht zum Systemabsturz.</p>
        </article>
    </div>
</div>
<div class="advantage">
    <h1 id="learn-more"><a href="https://www.rust-lang.org/">Geschrieben in Rust</a></h1>
    <div class="card">
        <h2>Speichersafe</h2>
        <p>
            Studien zeigen, dass <a href="https://en.wikipedia.org/wiki/Memory_safety#Impact">bis zu 70 % der Sicherheitslücken</a> auf schlechtes Speichermanagement zurückzuführen sind. Rust wurde für absolute Speichersicherheit entwickelt und eliminiert diese ganze Klasse von Fehlern effektiv zur Kompilierzeit und verhindert Pufferüberläufe, Use-after-free und Race Conditions.
        </p>
    </div>
    <div class="card">
        <h2>Für Systeme gemacht</h2>
        <p>
            Rust ist stark typisiert, verfügt über ein paralleles, aber dennoch sicherheitsorientiertes Design und bietet umfangreiche Prüfungen zur Kompilierungszeit. 
            Es ist Jahr für Jahr die <a href="https://www.technologyreview.com/2023/02/14/1067869/rust-worlds-fastest-growing-programming-language/">beliebteste</a> 
            und am meisten <a href="https://survey.stackoverflow.co/2023/#section-admired-and-desired-programming-scripting-and-markup-languages">bewunderte Sprache.</a>
        </p>
    </div>
    <div class="card">
        <h2>Schnell</h2>
        <article>
            <p>
                Rust ist auf Geschwindigkeit und Speichereffizienz ausgelegt und bietet Inline-Erweiterung, statisches Dispatch und Zero-Copy-Sharing.
            </p>
            <p>Die Leistung von Rust übertrifft oft die von C!</p>
        </article>
    </div>
</div>

<div class="advantage">
    <h1>Linux-Apps auf Redox</h1>
    <div class="card">
        <h2>Kosmischer Desktop</h2>
        <article>
            <p>
                <a href="https://blog.system76.com/post/hammering-out-cosmic-features">Cosmic Desktop</a> 
                ist eine neue Desktop-Umgebung für Linux, die derzeit von <a href="https://system76.com/">System76</a> entwickelt wird. Redox umfasst Cosmic-Files, Cosmic-Edit und Cosmic-Term, weitere sind geplant.
        </p></article>
    </div>
    <div class="card">
        <h2>In Rust geschriebene Alternativen</h2>
        <article>
            <p>
                Redox enthält viele Linux-Rust-Apps wie
                </p><ul>
                    <li><a href="https://github.com/uutils/coreutils">uutils</a> coreutils Alternativen</li>
                    <li><a href="https://helix-editor.com/">Helix-</a> Editor</li>
                    <li><a href="https://www.nushell.sh/">Nushell</a> -Befehlszeilen-Shell</li>
                </ul>
                ... und viele andere.
            <p></p>
        </article>
    </div>
    <div class="card">
        <h2>Zunehmende Kompatibilität mit Linux-Apps</h2>
        <article>
            <p>
                Redox bietet eine quellkompatible Schnittstelle für Linux- und BSD-Programme. Derzeit funktionieren Dutzende von Programmen reibungslos und über 1.000 Programme und Bibliotheken werden portiert.
            </p>
        </article>
    </div>
</div>

<div class="advantage">
    <h1>Open Source und kostenlos</h1>
    <div class="card">
        <h2>MIT-Lizenz</h2>
        <article>
            <p>
                Redox kann für jeden Zweck kostenlos verwendet werden. Es wird von einer Community von Freiwilligen entwickelt. 
                <a href="/donate">Unterstützen Sie unser Team!</a>
            </p>
        </article>
    </div>
    <div class="card">
        <h2>Open Source</h2>
        <article>
            <p>
                Die Redox-Software ist <a href="https://gitlab.redox-os.org/redox-os/redox">hier</a> . 
                Sie können an der Redox-Entwicklung teilnehmen, Fehler melden oder Vorschläge machen. Besuchen Sie uns im <a href="https://doc.redox-os.org/book/ch13-01-chat.html">Matrix-Chat,</a> wenn Sie etwas beitragen möchten.
            </p>
        </article>
    </div>
    <div class="card">
        <h2>Für immer frei</h2>
        <article>
            <p>
                Redox finanziert sich über Zuschüsse und Spenden. Redox wird von einer gemeinnützigen Organisation gemäß <a href="https://en.wikipedia.org/wiki/501(c)_organization#501(c)(4)">501(c)(4)</a> verwaltet .
            </p>
            <a href="./donate" class="button donate primary">Spenden</a>
        </article>
    </div>
</div>

<hr>

<div id="sponsors">
    <article class="hero">
        <h1>Sponsoren</h1>
        <p>Dank unserer großzügigen Sponsoren und Förderer können wir Programme wie unseren Redox Summer of Code finanzieren. Wir sind außerdem auf der Suche nach größeren Spenden, um unsere Entwicklungspläne zu beschleunigen.</p>        
        <a href="./donate" class="button donate primary">Spenden</a>
        <div id="sponsors-row" class="row">
            <a href="https://radworks.org" class="block"><img src="/img/logos/radworks.svg"></a>
        </div>
        <h2>Einzelne Sponsoren</h2>
        <p>Zu dieser Liste gehören natürlich auch die Einzelpersonen, die Spenden für das Redox OS-Projekt geleistet haben.</p>
    </article>
</div>
