+++
title ="捐赠/商品"
+++

## Redox OS 非营利组织

Redox OS 有一家在科罗拉多州(美国)注册成立的 501(c)(4) 非营利组织用来负责管理
捐款. 对 Redox OS 非营利组织的捐款将按照以下决定使用:
Redox OS 志愿者董事会.

您可以通过以下方式向 Redox OS 捐款:

  - [Patreon](https://www.patreon.com/redox_os)
  - [Donorbox](https://donorbox.org/redox-os)
  - 如需更多捐赠选项, 请联系 donate@redox-os.org

## 商品

我们在 Teespring 上出售T恤, 您可以[在这里](https://redox-os.creator-spring.com/)购买.

每笔销售都是对 Redox OS 非营利组织的捐赠.

## Jeremy Soller

Jeremy Soller 是 Redox OS 的创建者、维护者和首席开发人员.
给他的捐款被视为应税礼物, 它们将由他决定支配使用

您可以通过以下方式向 Jeremy 捐款:

- [Liberapay](https://liberapay.com/redox_os)
- [Paypal](https://www.paypal.me/redoxos)
- Jeremy 不再接受比特币或 Ethereum 捐赠. 不要向网站上以前列出的钱包地址捐款, 因为捐款将不会被收到.

以下赞助人已向 Jeremy 捐赠 4 美元及以上用于开发 Redox OS:
{{% partial "donors/jackpot51.html" %}}
