+++
title = "Documentation"
+++

<div>
    <div>
        <p>
            Redox's documentation is constantly evolving. We would love your help to write high-quality documentation.
        </p>
        <a href="#"><button>Contribute</button></a>
    </div>
    <div class="cards">
        <h2>Getting Started</h2>
        <article class="hero">
            <h3>Redox OS Book</h3>
            <p>The RedoxOS Book is a fantastic place to learn the fundamentals of Redox, and to begin understanding the system more deeply.</p>
            <p>We highly recommend this as your starting point for newcomers and as a general reference</p>
            <a href="#" class="button">Redox OS Book</a>
        </article>
    </div>
    <div class="cards">
        <h2>References</h2>
        <div class="card-container">
            <div class="card">
                <h3><code>redox_syscall</code></h3>
                <article>
                    <p>This crate wraps the raw syscalls to produce a friendly library you can use to implement schemes.</p>
                    <p>You likely will only need to use this library directly if you are implementing a scheme yourself. For regular Redox software, see <a href="#">ReLibC</a></p>
                    <a href="#" class="button"><code>redox_syscall</code></a>
                </article>
            </div>
            <div class="card">
                <h3><code>libstd</code></h3>
                <article>
                    <p>Rust Standard Library</p>
                    <blockquote class="info">
                        <p>
                            This crate is not maintained by Redox OS. It instead builds upon work which is. See <a href="#">ReLibC</a> for more
                        </p>
                    </blockquote>
                    <a href="#" class="button">Rust Standard Library</a>
                </article>
            </div>
            <div class="card">
                <h3>RFCs</h3>
                <article>
                    <p>Request changes to Redox</p>
                    <a href="" class="button">RFCs</a>
                </article>
            </div>
        </div>
    </div>
    <div class="cards">
        <h2>GitLab</h2>
        <div class="card-container">
            <div class="card">
                <h3>Redox OS</h3>
                <article>
                    <p>The root repository containing everything necessary to build Redox OS</p>
                    <a href="#" class="button">Redox OS</a>
                </article>
            </div>
            <div class="card">
                <h3>Drivers</h3>
                <article>
                    <p>Drivers power the peripherals and internal hardware the computer system depends on for its work.</p>
                    <p>Here, the source code for all the most important drivers is contained.</p>
                    <a href="#" class="button">Drivers</a>
                </article>
            </div>
            <div class="card">
                <h3>ion shell</h3>
                    <p>The ion shell is a posix-compliant Redox-native shell.</p>
                    <blockquote>
                        <p>
                            There is also a <a href="#">book</a> available for ion which is a great starting point for using Redox as a system. 
                            It also offers extensive developer-oriented extension.
                        </p>
                    </blockquote>
                    <a href="" class="button">ion Shell</a>
                    <a href="" class="button secondary">Book</a>
                </article>
            </div>
        </div>
    </div>
</div>

# Frequently Asked Questions

<details class="faq">
    <summary><h2>What is Redox</summary>
    <article>
        <p>Redox is a microkernel-based, complete, fully-functioning and general-purpose operating system created in 2015, with a focus on safety, freedom, reliability, correctness, and pragmatism.</p>
        <p>Wherever possible, the system components are written in Rust and run in user-space.</p>
    </article>
</details>

<details class="faq">
    <summary><h2>Where is Redox at?</h2></summary>
    <article>
        <p>Redox is alpha/beta quality software, because we implement new features while fixing the bugs.</p>
        <p>Because of this, it's not ready for daily usage yet. Feel free to test the system until its maturity and <b>don't store your sensitive data without a proper backup.</b></p>
        <p>The 1.0 version will be released once all system APIs are considered stable.</p>
    </article>
</details>

<details class="faq">
    <summary><h2>What does the name <i>Redox</i> mean?</h2></summary>
    <article>
        <p>A <a href="https://en.wikipedia.org/wiki/Redox">Redox</a> reaction is one which among other things causes the formation of Rust - which happens to be the name of the language Redox is written in.</p>
        <p>Oh and it sounds like Minix and Linux!</p>
    </article>
</details>

<details class="faq">
    <summary><h2>What is a microkernel?</h2></summary>
    <article>
        <p>A microkernel is the near-minimum amount of software that can provide the mechanisms needed to implement an operating system, which runs on the highest privilege of the processor.</p>
        <p>This approach to OS design brings more stability and security, with a small cost on performance.</p>
        <a href="https://doc.redox-os.org/book/ch04-01-microkernels.html" class="button secondary">Read More</a>
    </article>
</details>

<details class="faq">
    <summary><h2>Why a microkernel?</h2></summary>
    <article>
        <h3>True modularity</h3>
        <p>You can modify/change many system components without a system restart, similar to but safer than <a href="https://en.wikipedia.org/wiki/Kpatch">livepatching</a>.</p>
        <h3>Bug isolation</h3>
        <p>Most system components run in user-space on a microkernel system. Because of this, bugs in most system components won't <a href="https://en.wikipedia.org/wiki/Kernel_panic">crash the system/kernel</a>.</p>
        <h3>Restartless design</h3>
        <p>A mature microkernel changes very little (except for bug fixes), so you won't need to restart your system very often to update it.</p>
        <p>Since most of the system components are in userspace, they can be replaced on-the-fly, reducing downtime for server administrators.</p>
        <h3>Easy to develop and debug</h3>
        <p>Most of the system components run in userspace, simplifying testing/debugging.</p>
    </article>
</details>

<details class="faq">
    <summary><h2>Why Rust?</h2></summary>
    <article>
        <h3>Less prone to bugs</h3>
        <p>The restrictive syntax and compiler suggestions reduce the probability of bugs a lot.</p>
        <h3>Less vulnerable to data corruption</h3>
        <p>The Rust compiler prevents the programmer from causing memory errors and race conditions, which reduces the probability of data corruption bugs.</p>
        <h3>Improved security and reliability without significant performance impact</h3>
        <p>As the kernel is small, it uses less memory to do its work. The limited kernel code size helps us work towards a bug-free status. You may have heard of the <a href="https://en.wikipedia.org/wiki/KISS_principle">KISS Principle</a>.</p>
        <h3>Thread safety</h3>
        <p>The C/C++ support for thread-safety is quite fragile. As such, it is very easy to write a program that looks safe to run across multiple threads, but which introduces subtle bugs or security holes. If one thread accesses a piece of state at the same time that another thread is changing it, the whole program can exhibit some truly confusing and bizarre bugs.</p>
        <p>In Rust, this kind of bug is easy to avoid: the same type system that keeps us from writing memory unsafety prevents us from writing dangerous concurrent access patterns.</p>
    </article>
</details>

<details class="faq">
    <summary><h2>How does Redox hold up against other OSes?</h2></summary>
    <article>
        <p>You can see how Redox is compared to Linux, FreeBSD and Plan 9 on <a href="https://doc.redox-os.org/book/ch04-11-features.html">the features </a> page.</p>
    </article>
</details>

<details class="faq">
    <summary><h2>What is Redox' purpose?</h2></summary>
    <article>
        <p>The main goal of Redox is to be a general-purpose OS, while maintaining security, reliability and correctness.</p>
        <p>Redox aims to be an alternative to existing Unix systems (Linux/BSD), with the ability to run most Unix programs with only recompilation or minimal modifications.</p>
        <a href="https://doc.redox-os.org/book/ch01-01-our-goals.html" class="button secondary">Our goals</a>
    </article>
</details>

<details class="faq">
    <summary><h2>What can I do with Redox?</h2></summary>
    <article>
        <p>As a general-purpose operating system, you will be able to do almost any task on most devices with high performance/security.</p>
        <p>Redox is still under development, so our list of supported applications is currently limited, but growing.</p>
        <a href="https://doc.redox-os.org/book/ch01-04-redox-use-cases.html" class="button secondary">Use Cases</a>
    </article>
</details>

<details class="faq">
    <summary><h2>What does UNIX-Like mean?</h2></summary>
    <article>
        <p>
            Any OS compatible with the <a href="https://en.wikipedia.org/wiki/Single_UNIX_Specification">Single Unix Specification</a> and <a href="https://en.wikipedia.org/wiki/POSIX">POSIX</a> is considered a UNIX-like OS. 
            You can expect a <a href="https://en.wikipedia.org/wiki/Unix_shell">shell</a>, the <a href="https://en.wikipedia.org/wiki/Everything_is_a_file">"Everything is a File"</a> concept, multitasking and multiuser support.
        </p>
        <a href="https://en.wikipedia.org/wiki/Unix-like" class="button secondary">Read More</a>
    </article>
</details>

<details class="faq">
    <summary><h2>How Redox is inspired by other systems?</h2></summary>
    <article>
        <h3><a href="http://9p.io/plan9/index.html">Plan 9</h3></h3>
        <p>This Bell Labs OS brings the concept of "Everything is a File" to the highest level, doing all the system communication from the filesystem.</p>
        <ul>
            <li><a href="https://drewdevault.com/2022/11/12/In-praise-of-Plan-9.html">Drew DeVault explains the Plan 9</a></li>
            <li><a href="https://doc.redox-os.org/book/ch05-00-urls-schemes-resources.html">Plan 9's influence on Redox</a></li>
        </ul>
        <h3><a href="https://minix3.org/">Minix</a></h3>
        <p>
            The most influential Unix-like system with a microkernel. 
            It has advanced features such as system modularity, <a href="https://en.wikipedia.org/wiki/Kernel_panic">kernel panic</a> resistence, driver reincarnation, 
            protection against bad drivers and secure interfaces for <a href="https://en.wikipedia.org/wiki/Inter-process_communication">interprocess-communication</a>.
        </p>
        <a href="https://doc.redox-os.org/book/ch04-01-microkernels.html">How Minix influenced the Redox design</a>
        <h3><a href="https://sel4.systems/">seL4</a></h3>
        <p>The simplest and most performant microkernel of the world.</p>
        <p>Redox follow the same principle, trying to make the kernel-space small as possible (moving components to user-space and reducing the number of system calls, passing the complexity to user-space) and keeping the overall performance good (reducing the context switch cost).</p>
        <h3></h3>
        <p>This Unix <a href="https://en.wikipedia.org/wiki/Research_Unix">family</a> included several improvements on Unix systems and the open-source variants of BSD added many improvements to the original system (like Linux did).</p>
        <p>
            <a href="https://www.freebsd.org/">FreeBSD</a> is the most notable example, 
            Redox took inspiration from <a href="https://man.freebsd.org/cgi/man.cgi?capsicum(4)">Capsicum</a> 
            (a capability-based system) and <a href="https://en.wikipedia.org/wiki/Freebsd_jail">jails</a> 
            a sandbox technology) for the namespaces implementation.
        </p>
        <h3><a href="https://www.kernel.org/">Linux</a></h3>
        <p>The most advanced monolithic kernel and biggest open-source project of the world. It brought several improvements and optimizations to the Unix-like world.</p>
        <p>Redox tries to implement some of the many Linux performance improvements in a microkernel design.</p>
    </article>
</details>

<details class="faq">
    <summary><h2>What programs can Redox run?</h2></summary>
    <article>
        <p>Redox is designed to be source-compatible with most Unix, Linux and POSIX-compliant applications, only requiring compilation.</p>
        <p>Currently, most GUI applications require porting, as we don't support X11 or Wayland yet, however the following list of major software already works</p>
        <ul>
            <li>GNU Bash</li>
            <li>FFMPEG</li>
            <li>Git</li>
            <li>SDL2</li>
            <li>OpenSSL</li>
            <li>Mesa3D</li>
            <li>GCC</li>
            <li>LLVM</li>
        </ul>
        <a href="https://static.redox-os.org/pkg/x86_64-unknown-redox/" class="buttons">All software</a>
    </article>
</details>

<details class="faq">
    <summary><h2>How to install programs on Redox?</h2></summary>
    <article>
        <p>Redox has a package manager similar to `apt` (Debian) and `pkg` (FreeBSD), you can see how to use it on <a href="https://doc.redox-os.org/book/ch02-08-pkg.html">this page</a></p>
    </article>
</details>

<details class="faq">
    <summary><h2>Which Redox variants are there?</h2></summary>
    <article>
        <p>The Redox build system allows you to cusomise how your final Redox image looks. There are a few variations available containing different software, layout and features</p>
        <table>
            <thead>
                <tr>
                    <td>Variant</td>
                    <td>Purpose</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><code>server-minimal</code></td>
                    <td>The most minimal variant with a basic system. Aimed at embedded devices, very old computers and developers.</td>
                </tr>
                <tr>
                    <td><code>desktop-minimal</code></td>
                    <td>The most minimal variant with the Orbital desktop environment included. Aimed at embedded devices, very old computers and developers.</td>
                </tr>
                <tr>
                    <td><code>server</code></td>
                    <td>The server variant with a complete system and network tools. Aimed at server administrators, embedded devices, low-end computers and developers.</td>
                </tr>
                <tr>
                    <td><code>desktop</code></td>
                    <td>The standard variant with a complete system, Orbital desktop environment and useful tools. Aimed at daily usage, producers, developers and gamers.</td>
                </tr>
                <tr>
                    <td><code>dev</code></td>
                    <td>The development variant with a complete system and development tools. Aimed at developers.</td>
                </tr>
                <tr>
                    <td><code>demo</code></td>
                    <td>The demo variant with a complete system, tools, players and games. Aimed at testers, gamers and developers.</td>
                </tr>
            </tbody>
        </table>
    </article>
</details>

<details class="faq">
    <summary><h2>Which devices does Redox support?</h2></summary>
    <article>
        <p>There are billions of devices with hundreds of models and architectures in the world. We try to write drivers for the most used devices to support more people. Support depends on the specific hardware, since some drivers are device-specific and others are architecture-specific.</p>
        <p>Have a look at <a href="https://gitlab.redox-os.org/redox-os/redox/-/blob/master/HARDWARE.md">Hardware</a> to see all tested computers.</p>
        <h3>CPU</h3>
        <ul>
            <li>Intel - 64-bit (x86_64) and 32-bit (i686) from Pentium II and after with limitations.</li>
            <li>AMD - 64-bit (AMD64) and 32-bit.</li>
            <li>ARM - 64-bit (Aarch64) with limitations.</li>
        </ul>
        <h3>Hardware interfaces</h3>
        <ul>
            <li>ACPI</li>
            <li>PCI</li>
            <li><i>USB coming soon</i></li>
        </ul>
        <h3>Video</h3>
        <ul>
            <li>VGA (BIOS)</li>
            <li>GOP (UEFI)</li>
            <li><a href="https://docs.mesa3d.org/drivers/llvmpipe.html">LLVMpipe</a> (OpenGL CPU emulation)</li>
            <li><i>Intel/AMD and others in the future</i></li>
        </ul>
        <h3>Audio</h3>
        <ul>
            <li>Intel chipsets</li>
            <li>Realtek chipsets</li>
            <li>PC speaker</li>
            <li><i>SoundBlaster coming soon</i></li>
        </ul>
        <h3>Storage</h3>
        <ul>
            <li>IDE (PATA)</li>
            <li>SATA (AHCI)</li>
            <li>NVMe</li>
            <li><i>USB coming soon</i></li>
        </ul>
        <h3>Input</h3>
        <ul>
            <li>PS/2 keyboards, mice and touchpads</li>
            <li><i>USB coming soon</i></li>
        </ul>
        <h3>Networking</h3>
        <ul>
            <li>Intel Gigabit ethernet</li>
            <li>Intel 10 Gigabit ethernet</li>
            <li>Realtek ethernet</li>
            <li><i>WiFi and Atheros ethernet coming soon</i></li>
        </ul>
    </article>
</details>

<details class="faq">
    <summary><h2>I have a low-end computer, would Redox work on it?</h2></summary>
    <article>
        <p>A CPU is the most complex machine of the world: even the oldest processors are powerful for some tasks but not for others.</p>
        <p>The main problem with old computers is the amount of RAM available (they were sold in a era where RAM chips were expensive) and the lack of SSE/AVX extensions (programs use them to speed up the algorithms). Because of this some modern programs may not work or require a lot of RAM to perform complex tasks.</p>
        <p>Redox itself will work normally if the processor architecture is supported by the system, but the performance and stability may vary per program.</p>
    </article>
</details>

<details class="faq">
    <summary><h2>What VMs can I run Redox on?</h2></summary>
    <article>
        <p>Currently, Redox' build system integrates only with Qemu, but VirtualBox works too. Any others are not supported, so your mileage may vary</p>
        <p>In the future the microkernel could act as a hypervisor, similar to <a href="https://xenproject.org/">Xen</a>.</p>
    </article>
</details>

- Read the [CONTRIBUTING.md](https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md)
