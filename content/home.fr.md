+++
title = "Accueil"
url = "home"
+++

<!--Two-Space indentation is necessary here 🤮🤮 -->

<div id="sponsors">
  <h1>Sponsors</h1>
  <p>Sans le soutien généreux et continu de nos sponsors, la poursuite du développement de Redox OS serait un véritable défi. Nous sommes extrêmement reconnaissants à chacun des sponsors mentionnés ci-dessous.</p>
  
  <div class="row">
    <p>Personne pour l'instant <a href="/donate">Soyez le premier !</a></p>
  </div>
  
  <a href="./donate"><button class="donate">Faire un don</button></a>
  
  <h2>Sponsors individuels</h2>
  <p>À cette liste s'ajoutent également les individus qui ont fait des dons au projet Redox OS.</p>
</div>

<hr />

<div>
  <h1>Qu'est-ce qu'est Redox</h1>
  
  <p>Redox est un système d'exploitation libre, gratuit et open source écrit en Rust.</p>
  <p>L'utilisation d'un micro-noyau nous permet de nous concentrer sur la sûreté, la sécurité et la stabilité</p>
  
  <a href="#learn-more"><button class="external">En Savoir Plus</button></a>
  <a href="https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md"><button class="gitlab">Participez !</button></a>
  <a href="/donate"><button class="donate">Faire un don</button></a>
</div>

<div class="advantage">
  <h1 id="learn-more">Protégé avec Rust</h1>
  <h2>Élimine efficacement les bugs</h2>
  <p>La sûreté à la compilation et la syntaxe restrictive de Rust éliminent efficacement les bugs liés à la mémoire.</p>

  <div class="card">
    <h2>Plus résistant à la corruption des données</h2>
    <article>
      <p>Le compilateur Rust rend presque impossible la corruption de la mémoire, ce qui rend Redox beaucoup plus fiable et stable.</p>
    </article>
  </div>
  
  <h2>Une meilleure sûreté, une meilleure sécurité</h2>
  <p>Les problèmes de sécurité mémoire peuvent représenter 70% des vulnérabilités<a><sup>1</sup></a>. Rust empêche ces problèmes, rendant Redox plus sûr et plus sécurisé par design.</p>
  
  <div class="card">
    <h2>Maintien de la sûreté des tâches multiples</h2>
    <article>
      <p>
C/C++ peuvent avoir des difficultés avec les données lorsqu'ils exécutent plusieurs tâches à la fois. 
Cela peut entraîner des problèmes ou des dangers sournois. Rust évite ces problèmes en vérifiant les données avant qu'elles ne soient utilisées ou modifiées.
      </p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>Une architecture en micro-noyau</h1>
  <div class="card">
    <h2>Plus de sécurité par moins de privilèges</h2>
    <article>
      <p>
Un micro-noyau contient le strict minimum de programmes permettant de fournir les mécanismes nécessaires à l'implémentation d'un système d'exploitation, celui-ci est exécuté avec les privilèges les plus élevés du processeur.
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>Stabilité avec l'isolation des bugs</h2>
    <article>
      <p>
Les composants du système qui s'exécutent dans l'espace utilisateur sont isolés du noyau du système. De ce fait, la plupart des bugs n'affecteront pas l'ensemble du système et ne le feront pas planter.
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>Un design modulaire et sans redémarrage</h2>
    <article>
      <p>
      Un micro-noyau stable change peu et les redémarrages sont rares. La plupart des éléments système de Redox se trouvent dans l'espace utilisateur, ils peuvent donc être remplacés à la volée et aucun redémarrage du système n'est nécessaire.
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>Facilité de développement et de débuggage</h2>
    <article>
      <p>Redox facilite le développement et l'amélioration des logiciels. Les éléments systèmes sont séparées du micro-noyau, ce qui simplifie les tests et le débuggage.</p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>Applications Linux sur Redox</h1>
  <div class="card">
    <h2>Relibc : Un Pont Linux-Redox OS</h2>
    <article>
      <p>Relibc est une bibliothèque spéciale qui permet à Redox OS d'exécuter de nombreuses applications Linux. Elle agit comme un pont entre le langage des applications Linux et Redox OS.</p>
    </article>
  </div>
  
  <div class="card">
    <h2>Compatibilité croissante avec les applications Linux</h2>
    <article>
      <p>Actuellement, des dizaines de programmes et beaucoup plus de bibliothèques fonctionnent sans effort. Plus de 1 000 programmes et bibliothèques sont actuellement portés sur Redox.</p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>Open source, libre et gratuit</h1>
  
  <div class="card">
    <h2>Exécuter librement n'importe quel logiciel</h2>
    <article>
      <p>Utilisez Redox OS et exécutez n'importe quel logiciel compatible pour n'importe quel usage, sans vous soucier des problèmes de source ou de licence.</p>
    </article>
  </div>

  <div class="card">
    <h2>Corriger les erreurs ou les signaler</h2>
    <article>
      <p>Tout le monde peut vérifier la qualité et la sécurité du code source de Redox, l'améliorer ou signaler les problèmes de bugs aux développeurs.</p>
    </article>
  </div>
  
  <div class="card">
    <h2>Gratuit et le restera toujours</h2>
    <article>
      <p>Aucune entreprise ne possède ou ne contrôle Redox OS. Redox OS est un projet à but non lucratif qui fonctionne grâce à des subventions et des dons.</p>
    </article>
  </div>
</div>

<!-- Latest News -->

<hr />

<h1>Dernières actualités</h1>

<p>Rien ici pour l'instant</p>
